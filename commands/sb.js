const fs = require('fs');
module.exports = {
    description: 'Plays a sound from the soundboard',
    example: '!sb <sound-name> <optional-volume 1-100>',
    action: function (command) {
        let args = command.content.substring(1).split(' ').slice(1);
        if (!command.guild) return;
        if (args.length === 0) {
            command.reply('Give me a sound to play!');
            return;
        }
        fs.readdir('./soundboard', function (err, files) {
            if (err) {
                console.log(err);
                return;
            }
            let filteredFiles = files.filter((cur) => {
                return cur.indexOf(args[0]) > -1;
            });
            if (filteredFiles.length === 0) {
                command.reply('I couldn\'t not find the sound you asked for :(');
                return;
            }

            if (command.member.voice.channel &&
                (!this.currentConnection || command.member.voice.channel.name !== this.currentConnection.channel.name)) {
                command.member.voice.channel.join()
                    .then(connection => {
                        this.currentConnection = connection;
                        playSoundBite(connection);
                    })
                    .catch(console.log);
            } else {
                playSoundBite(this.currentConnection);
            }

            function playSoundBite(connection) {
                let dispatcher = connection.play(process.cwd() + '/soundboard/' + filteredFiles[0]);
                if (args.length > 1) {
                    let volume = parseFloat(args[1]);
                    if (volume > 100) {
                        volume = 100;
                    }
                    dispatcher.setVolume(volume / 100.00);
                } else {
                    let volume = 10;
                    dispatcher.setVolume(volume / 100.00);
                }
            }
        });
    }
}